# Feature Request

**Hinweis 1**: Dieser Issue ist vertraulich. Bitte überlege gut, ob der Issue veröffentlicht werden kann. Hier sind die Regeln zur Veröffentlichung zu beachten.

**Hinweis 2**: Standardtexte aus dem Template sind entsprechend zu entfernen.

## Neue Fähigkeiten

*Beschreibe hier die neue Fähigkeit im Detail. Nutze alle Möglichkeiten der Darstellung. Es können auch Bilder und Videos eingebunden werden.*

## Voraussetzungen

*Ist für das Feature Hardware erfordlich? Wenn ja, beschreibe diese.*
*Gibt es Verknüpfungen mit anderen Fähigkeiten? Wenn ja, beschreibe diese.*

## Lösungsvorschläge

*Du hast einen Lösungsvorschlag für das neue Feature? Dann beschreibe es hier oder Lösche das Kapitel*

## Abnahmekriterien

*Beschreibe die Anforderungen, welche über die Standard Design Rules hinaus gehen.*
*Wann gilt das Feature als fertig.*


<!-- Config -->
/confidential
/label ~"type::new feature"
