# Bug Report

**Hinweis 0**: Wenn der Fehler zum vollständigen Stillstand führt, dann bitte umgehend beim Ansprechpartner der Häcker Automation melden. (Service, Applikation, Software-Abteilung)*

**Hinweis 1**: Dieser Issue ist vertraulich. Bitte überlege gut, ob der Issue veröffentlicht werden kann. Hier sind die Regeln zur Veröffentlichung zu beachten.

**Hinweis 2**: Standardtexte aus dem Template sind entsprechend zu entfernen.

## Fehlerbeschreibung

*Beschreibe die Ausgangslage, das Fehlerbild, mögliche Loginformationen und weitere Gegebenheiten. Weitere Informationen findest du [hier](https://inside.haecker-automation.net/ourplant/software/documents/development-environment/-/blob/main/development%20process/bugfixing_information.md).*

## Einordnung des Fehlers

*Wenn der Fehler reproduzierbar ist, beschreibe wie.*
*Tritt der Fehler sporadich auf, dann beschreibe, wie oft das im laufenden Betrieb passiert.*
*Wie beeinflusst der Fehler das Verhalten? [von Maschinengefährdung bis Ergonomieproblem]*

## korrektes Verhalten

*Kennst du das korrekte Verhalten? Wie müsste es sein?*

## Zusatzmaterial

*Stelle Zusatzmaterial wie Logausschriften oder Screenshots innerhalb des Issues zur Verfügung.*

## Sonstiges

*Welche weiteren Informationen gibt es? Was sollte ggf. noch beachtet werden?*


/confidential
/label ~type::bug
