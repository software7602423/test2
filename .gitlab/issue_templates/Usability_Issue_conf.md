# Usability Issue

**Hinweis 1**: Dieser Issue ist vertraulich. Bitte überlege gut, ob der Issue veröffentlicht werden kann. Hier sind die Regeln zur Veröffentlichung zu beachten.

**Hinweis 2**: Standardtexte aus dem Template sind entsprechend zu entfernen.

## Problembeschreibung der aktuellen Umsetzung

*Beschreibe hier das Problem. Sollte dies die grafische Oberfläche der Software betreffen, so nutze bitte Screenshots (lassen sich via Copy&Paste einfügen). Sind abläufe betroffen, dann nutze bitte FlowCharts (hierzu kann entweder die [Mermaid-Engine](https://docs.gitlab.com/ee/user/markdown.html#mermaid) genutzt werden oder Bilder in einem externen Editor, z.B. DrawIO, erstellt werden). 

## Anforderungen

*Beschreibe wie es sein sollte.*

## Umsetzungsvorschlag

*Wie und wo sollte das System umgesetzt werden?*

## Abnahmekriterien

*Wann gilt das neue System als fertig?*

/confidential
/label ~"type::usability"
